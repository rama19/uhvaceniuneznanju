## KVIZ -  UHVAĆENI U NEZNANJU
[![Build Status](https://semaphoreci.com/api/v1/projects/6086e48a-fac6-4235-9b63-20331351bdb9/843892/badge.svg)](https://semaphoreci.com/rama19/uhvaceniuneznanju)

Getting started

Make sure you have:

1. Proper Ruby installed (check .ruby-version)
2. PostgreSQL database installed
3. Bundler gem installed (gem install bundler)
4. Foreman gem installed (gem install foreman)

Then do following:

~~~~
git clone git@bitbucket.org:rama19/uhvaceniuneznanju.git
cd uhvaceniuneznanju
bundle install
cp env.example .env # See [1] for more details
rake db:create
rake db:schema:load
foreman start
open http://localhost:5000
~~~~

[1] Enter/update admin credentials and Cloudinary secrets

### Repository

  Code is on [BitBucket](https://bitbucket.org/rama19/uhvaceniuneznanju)

### Deploy

  To deploy to heroku use `git push heroku master`

### Monitoring

  Site is monitored by [UptimeRobot](https://uptimerobot.com/dashboard#776927863)
