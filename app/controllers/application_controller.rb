require "application_responder"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_admin!

  def toast(type, text)
    flash[:toast] = { type => text }
  end

  private

  def authenticate_admin!
    authenticate_or_request_with_http_basic do |username, password|
      expected_username = ENV.fetch("APP_USERNAME")
      expected_password = ENV.fetch("APP_PASSWORD")
      username == expected_username && password == expected_password
    end
  end

end

