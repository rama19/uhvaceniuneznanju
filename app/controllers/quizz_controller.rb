class QuizzController < ApplicationController

  def home
  end

  def quizz
    @categories = Category.all
    @questions = Question.all
  end

  def show
    @question = Question.find_by_id(params[:question_id])
    @category = @question.category
    @teams = Team.all.order("created_at DESC")
  end
end

