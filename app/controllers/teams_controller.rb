class TeamsController < ApplicationController

  def index
    @teams = Team.all
  end

  def new
    @team = Team.new
  end

  def create
    @team = Team.new(team_params)

    if @team.save
      redirect_to teams_path, notice: "Ekipa uspješno dodana!"
    else
      render "new"
    end
  end

  def edit
    @team = Team.find(params[:id])
  end

  def update
    @team = Team.find(params[:id])
    respond_to do |format|
      if @team.update(team_params)
        format.html { redirect_to(teams_path, notice: "Izmjene uspješno spremljene!") }
      else
        format.html { render "edit" }
      end
    end
  end

  def destroy
    @team = Team.find(params[:id])
    @team.destroy

    redirect_to teams_path, notice: "Ekipa izbrisana!"
  end

  private

  def team_params
    params.require(:team).permit(:name, :game1, :game2, :game3, :game4, :game5, :game6, :game7, :game8, :game9, :game10, :game11, :game12,
                                 :game13, :game14, :game15)
  end

end

