module ApplicationHelper

  def main_menu_items
    [
      ["Početna", root_path],
      ["Započni kviz", quizz_path]
    ]
  end

  def render_main_menu
    items = main_menu_items
    return "" if items.empty?
    lis = items.map do |item|
      li_class = current_page?(item[1]) ? "active" : nil
      link = link_to(item[0], item[1])
      content_tag(:li, link, class: li_class)
    end
    content_tag(:ul, lis.join("").html_safe, class: "right")
  end

  def side_menu_items
    [
      ["Uhvaćeni u neznanju!"],
      ["Kategorije", categories_path],
      ["Pitanja", questions_path],
      ["Ekipe", teams_path],
      ["Dodaj kategoriju", new_category_path],
      ["Dodaj pitanje", new_question_path],
      ["Dodaj ekipu", new_team_path]
    ]
  end

  def render_side_menu
    items = side_menu_items
    return "" if items.empty?
    lis = items.map do |item|
      li_class = current_page?(item[1]) ? "active" : nil
      link = link_to(item[0], item[1])
      content_tag(:li, link, class: li_class)
    end
    content_tag(:ul, lis.join("").html_safe, id: "slide-out", class: "side-nav")
  end

end

