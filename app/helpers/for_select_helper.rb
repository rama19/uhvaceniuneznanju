module ForSelectHelper

  def categories_for_select
    Category::NAME.map { |key| [Category::Name.new(key).human_name, key] }
  end
end

