module QuestionsHelper
  QUESTION_IMAGE_OPTIONS = {
    small: { width: 20, height: 20 },
    medium: { width: 300, height: 400 },
    large: { width: 450, height: 550 }
  }.freeze

  def question_image_tag(question, size)
    image_path = question_image_path(question, size)
    options = QUESTION_IMAGE_OPTIONS[size]
    cl_image_tag(image_path, options)
  end

  def question_image_path(question, size)
    if question.image.present?
      question.image.path
    else
      asset_path("question/fallback/#{size}.png")
    end
  end
end

