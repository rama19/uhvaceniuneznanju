module QuizzHelper
  def md_to_html(text)
    options = {
      filter_html: false,
      fenced_code_blocks: true,
      gh_blockcode: true,
      hard_wrap: true
    }
    extensions = {
      autolink: true,
      no_intra_emphasis: true
    }
    renderer = Redcarpet::Render::HTML.new(options)
    md_for_html = Redcarpet::Markdown.new(renderer, extensions)
    md_for_html.render(text).html_safe
  end
end

