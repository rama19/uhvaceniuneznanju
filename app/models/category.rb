class Category < ActiveRecord::Base
  has_many :questions

  NAME = %w(history geography art literature comics movie music culture religion mithology sport technology science politics).freeze

  validates :name, presence: true
end

