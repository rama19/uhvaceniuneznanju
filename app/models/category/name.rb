class Category
  class Name

    def initialize(key)
      @key = key
    end

    def human_name
      I18n.t("models.category.name.#{@key}")
    end

  end
end

