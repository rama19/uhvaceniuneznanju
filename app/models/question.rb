class Question < ActiveRecord::Base
  belongs_to :category

  has_attachment :image, accept: [:gif, :jpg, :jpeg, :png]

  validates :question_name, presence: true
  validates :answer, presence: true
  validates :image, presence: true
end

