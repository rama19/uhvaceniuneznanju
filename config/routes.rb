Rails.application.routes.draw do
  mount Attachinary::Engine => "/attachinary"

  root to: "quizz#home"

  %w(quizz
     show).each do |page|
    get page, to: "quizz##{page}", as: page
  end

  resources :categories
  resources :questions
  resources :teams
end

