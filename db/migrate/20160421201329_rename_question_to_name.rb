class RenameQuestionToName < ActiveRecord::Migration
  def up
    rename_column :questions, :question, :question_name
  end

  def down
    rename_column :questions, :question_name, :question
  end
end

