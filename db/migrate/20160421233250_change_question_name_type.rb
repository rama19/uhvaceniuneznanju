class ChangeQuestionNameType < ActiveRecord::Migration
  def up
    change_column :questions, :question_name, :string
  end

  def down
    change_column :questions, :question_name, :text
  end
end

