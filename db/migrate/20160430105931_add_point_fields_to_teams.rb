class AddPointFieldsToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :game1, :integer, default: 0
    add_column :teams, :game2, :integer, default: 0
    add_column :teams, :game3, :integer, default: 0
    add_column :teams, :game4, :integer, default: 0
    add_column :teams, :game5, :integer, default: 0
    add_column :teams, :game6, :integer, default: 0
    add_column :teams, :game7, :integer, default: 0
    add_column :teams, :game8, :integer, default: 0
    add_column :teams, :game9, :integer, default: 0
    add_column :teams, :game10, :integer, default: 0
  end
end

