class AddPointsSumToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :sum, :integer, default: 0
  end
end

