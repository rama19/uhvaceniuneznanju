class AddingGamesToTeams < ActiveRecord::Migration
  def change
    add_column :teams, :game11, :integer, default: 0
    add_column :teams, :game12, :integer, default: 0
    add_column :teams, :game13, :integer, default: 0
    add_column :teams, :game14, :integer, default: 0
    add_column :teams, :game15, :integer, default: 0
  end
end

